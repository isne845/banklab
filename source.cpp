#include<iostream>
#include<vector>
#include<cstdlib>
#include<ctime>

using namespace std;

int randomNum(int mn, int mxn) { 
	int n = rand() % mxn + mn; //random number in the range of mn to mxn
	return n;
}


class customer {
public:
	int arriveTime=0;
	int service=0;
	int waitTime=0;
	int lefttime=0; //time when customer left
};

class cashier {
public:
	int lefttime=0; //time when previous customer left from this cashiers
};

float waiting(vector <customer> wcus) {
	float wait = 0;
	for (int i = 0; i < wcus.size(); i++) {
		wait = wait + wcus[i].waitTime;
	}
	return wait;
}


int main() {
	int ms; //min service
	int	mxs; //max service
	int mc; //min customer
	int mxc; //max customer
	int num_cash; //number of cashier 
	int num_cus; //number of customer
	float wait; //average waiting time for customer

	cout << "\nInput the value"; 
	
	cout << "\nMaxCustomers : ";
	cin >> mxc;
	cout << "\nMinCustomers : ";
	cin >> mc;
	cout<<	"\nMaxServiceTime : ";
	cin >> mxs;
	cout << "\nMinServiceTime : ";
	cin >> ms;
	cout << "\nNumCashiers : ";
	cin >> num_cash;

	

	vector<customer>cus; //group of customer
	vector<cashier>cash; //group of cashier

	for (int i = 0; i < num_cash; i++) { //set initial time of each cashier
		cashier tmp;
		tmp.lefttime = 0;
		cash.push_back(tmp);
	}

	do {
		num_cus = randomNum(mc, mxc); //random amount of customer in the range of min and max customer
	} while (num_cus > mxc);

	for (int i = 0; i < num_cus; i++) { //data of each customer
		customer tmp;
		tmp.arriveTime = rand() % 241; //random arrive time of customer

		do {
			tmp.service = randomNum(ms, mxs); //random service time per each customer
		} while (tmp.service > mxs);

		cus.push_back(tmp);
	}

	//sort customer by arrive time
	for (int i = 0; i < num_cus; i++) {
		for (int j = 0; j < num_cus; j++) {
			if (cus[i].arriveTime < cus[j].arriveTime) {
				int a, s, w, l; //temp data

				a = cus[i].arriveTime;
				s = cus[i].service;
				w = cus[i].waitTime;
				l = cus[i].lefttime;

				//swap
				cus[i].arriveTime = cus[j].arriveTime;
				cus[i].service = cus[j].service;
				cus[i].waitTime = cus[j].waitTime;
				cus[i].lefttime = cus[j].lefttime;

				cus[j].arriveTime = a;
				cus[j].service = s;
				cus[j].waitTime = w;
				cus[j].lefttime = l;	
			}
		}
	}


	//calulate wait time of each customer
	for(int i = 0; i < num_cash; i++) {
		cus[i].waitTime = 0;
		cus[i].lefttime = cus[i].arriveTime + cus[i].service;
		cash[i].lefttime = cus[i].lefttime;

	}
	
	for (int i = num_cash; i < num_cus; i++) {
		int w; //wait of cashier
		int c; //which cashier

		//dicision cashier
		//first cashier
		if (cus[i].arriveTime > cash[0].lefttime) {
			w = 0;
			c = 0;
		}
		else {
			w = cash[0].lefttime - cus[i].arriveTime;
			c = 0;
		}
		//other cashier
		for (int j=0; j < num_cash; j++) {
			if (cus[i].arriveTime > cash[j].lefttime) {
				w = 0;
				c = j;

			}
			else {
				if (w > cash[j].lefttime - cus[i].arriveTime) {
					w = cash[j].lefttime - cus[i].arriveTime;
					c = j;
				}
			}
		}

		cus[i].waitTime = w;
		cus[i].lefttime = cus[i].arriveTime + cus[i].service + w;
		cash[c].lefttime = cus[i].lefttime;
	}
	
	for (int i = 0; i < num_cus; i++) {
		cout<< "\nCustomer no. "<<i+1 << endl << "Arrived at: " << cus[i].arriveTime<< " Wait Time: "<< cus[i].waitTime<< " Left: "<< cus[i].lefttime << endl;
	}

	cout << "\n\nAverage Wait Time: " << waiting(cus) / num_cus;
	
}